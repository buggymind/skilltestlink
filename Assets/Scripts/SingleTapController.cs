using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTapController : MonoBehaviour
{
    private GameNode gameNode;

    private void Awake()
    {
        gameNode = GetComponentInChildren<GameNode>(true);
    }

    void Start()
    {
        EventBus.Register(Events.Input.MouseDown, OnMouseDown);
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Input.MouseDown, OnMouseDown);
    }

    private void OnMouseDown(object[] data)
    {
        Transform targetTransform = (Transform)data[0];

        if (targetTransform == gameNode.transform)
            FinishTap();
    }

    private void FinishTap()
    {
        gameNode.ShowSuccess();
        EventBus.Push(Events.Game.LinkFinished);
    }
}
