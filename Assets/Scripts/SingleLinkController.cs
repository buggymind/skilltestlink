using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleLinkController : MonoBehaviour
{
    [Header("Global references")]
    public BloodlineController BloodlinePrefab;

    private List<GameNode> gameNodes;
    private BloodlineController bloodlineController;

    private bool linkStarted;
    private bool linkFinished;

    private void Awake()
    {
        FindGameNodesOnGameObject();
    }

    void Start()
    {
        EventBus.Register(Events.Input.MouseDown, OnMouseDown);
        EventBus.Register(Events.Input.MouseHold, OnMouseHold);
        EventBus.Register(Events.Input.MouseUp, OnMouseUp);

        InitBloodline();
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Input.MouseDown, OnMouseDown);
        EventBus.Unregister(Events.Input.MouseHold, OnMouseHold);
        EventBus.Unregister(Events.Input.MouseUp, OnMouseUp);
    }

    private void OnMouseDown(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNode.Type == GameNode.Types.Start)
            if (!linkStarted)
                StartLink();
    }

    private void OnMouseHold(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNode.Type == GameNode.Types.End)
            if (gameNodes.Contains(gameNode))
                if (linkStarted && !linkFinished)
                    FinishLink();
    }
    private void OnMouseUp(object[] data)
    {
        linkStarted = false;
    }

    private void FindGameNodesOnGameObject()
    {
        gameNodes = new List<GameNode>(GetComponentsInChildren<GameNode>(true));
    }

    private void InitBloodline()
    {
        bloodlineController = Instantiate(BloodlinePrefab, this.transform, true);

        var gameNodeStart = gameNodes.Find(gn => gn.Type == GameNode.Types.Start);
        var gameNodeEnd = gameNodes.Find(gn => gn.Type == GameNode.Types.End);

        bloodlineController.Init(gameNodeStart.transform, gameNodeEnd.transform, Color.gray);
    }

    private void StartLink()
    {
        linkStarted = true;
    }

    private void FinishLink()
    {
        linkFinished = true;
        gameNodes.ForEach(gn => gn.ShowSuccess());
        Destroy(bloodlineController.gameObject);

        EventBus.Push(Events.Game.LinkFinished);
    }
}
