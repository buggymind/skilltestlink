using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrippleTapController : MonoBehaviour
{
    private const int RequiredTaps = 3;

    public SpriteRenderer SpriteRenderer;
    public List<Sprite> HeartSprites;

    private GameNode gameNode;

    private int tapsFinished = 0;

    private void Awake()
    {
        gameNode = GetComponentInChildren<GameNode>(true);
    }

    void Start()
    {
        EventBus.Register(Events.Input.MouseDown, OnMouseDown);
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Input.MouseDown, OnMouseDown);
    }

    private void OnMouseDown(object[] data)
    {
        Transform targetTransform = (Transform)data[0];

        if (targetTransform != gameNode.transform)
            return;

        AddTap();

        if (tapsFinished == RequiredTaps)
            FinishTrippeTap();
    }

    private void AddTap()
    {
        SpriteRenderer.sprite = HeartSprites[tapsFinished];
        tapsFinished++;
    }

    private void FinishTrippeTap()
    {
        gameNode.ShowSuccess();
        EventBus.Push(Events.Game.LinkFinished);
    }
}
