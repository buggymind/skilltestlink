using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUIController : MonoBehaviour
{
    public ScoreScreenViewController ScoreScreen;
    public TipsDataScriptableObject TipsData;

    void Start()
    {
        EventBus.Register(Events.Game.LevelFinished, OnLevelFinished);
    }

    void OnDestroy()
    {
        EventBus.Unregister(Events.Game.LevelFinished, OnLevelFinished);
    }

    private void OnLevelFinished(object[] data)
    {
        int collectedBlood = (int)data[0];
        int saveGameCollectedBlood = (int)data[1];

        ScoreScreen.gameObject.SetActive(true);

        //if (collectedBlood > saveGameCollectedBlood)
            ScoreScreen.SetScore(collectedBlood);
        //else
            //ScoreScreen.SetScore(saveGameCollectedBlood);

        string tip = GetRandomTip();
        ScoreScreen.SetTip(tip);
    }

    private string GetRandomTip()
    {
        int randomIndex = Random.Range(0, 6);
        return TipsData.Tips[randomIndex];
    }
}
