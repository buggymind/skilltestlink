using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiLinkController : MonoBehaviour
{
    private List<GameNode> gameNodes;

    private int linkedNodes;

    private void Awake()
    {
        FindGameNodesOnGameObject();
    }

    void Start()
    {
        EventBus.Register(Events.Input.MouseDown, OnMouseDown);
        EventBus.Register(Events.Input.MouseHold, OnMouseHold);
        EventBus.Register(Events.Input.MouseUp, OnMouseUp);
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Input.MouseDown, OnMouseDown);
        EventBus.Unregister(Events.Input.MouseHold, OnMouseHold);
        EventBus.Unregister(Events.Input.MouseUp, OnMouseUp);
    }

    private void OnMouseDown(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNodes.Contains(gameNode))
            if (gameNode.Type == GameNode.Types.Multi)
                SetNodeAsLinked(gameNode);
    }

    private void OnMouseHold(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNodes.Contains(gameNode))
            if (gameNode.Type == GameNode.Types.Multi)
                if (!gameNode.IsFinished)
                    if (linkedNodes >= gameNode.RequiredLinks)
                        SetNodeAsLinked(gameNode);
    }

    private void OnMouseUp(object[] data)
    {
        Reset();
    }

    private void Reset()
    {
        linkedNodes = 0;
        gameNodes.ForEach(gn => gn.Reset());
    }

    private void FindGameNodesOnGameObject()
    {
        gameNodes = new List<GameNode>(GetComponentsInChildren<GameNode>(true));
    }

    private void SetNodeAsLinked(GameNode gameNode)
    {
        gameNode.IsFinished = true;
        gameNode.ShowLinked();
        linkedNodes++;

        if (linkedNodes == gameNodes.Count)
            FinishMultiLink();
    }

    private void FinishMultiLink()
    {
        gameNodes.ForEach(gn => gn.ShowSuccess());

        EventBus.Push(Events.Game.LinkFinished);
    }
}
