using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OptionsController : MonoBehaviour
{
    [Header("References")]
    public AudioMixer AudioMixer;

    [Header("Option elements")]
    public OptionElementView SoundOptionView;
    public OptionElementView MusicOptionView;

    [Header("Settings")]
    [Range(-80, 20)]
    public float MusicVolumeMin;
    [Range(-80, 20)]
    public float MusicVolumeMax;
    [Range(-80, 20)]
    public float SoundVolumeMin;
    [Range(-80, 20)]
    public float SoundVolumeMax;

    private Options options;

    private void Start()
    {
        SoundOptionView.Button.onClick.AddListener(OnSoundOptionClick);
        MusicOptionView.Button.onClick.AddListener(OnMusicOptionClick);

        options = DataManager.Instance.GetOptions();

        SoundOptionView.ShowOn(options.Sound);   
        MusicOptionView.ShowOn(options.Music);

        UpdateSound();
        UpdateMusic();
    }

    private void OnDestroy()
    {
        SoundOptionView.Button.onClick.RemoveListener(OnSoundOptionClick);
        MusicOptionView.Button.onClick.RemoveListener(OnMusicOptionClick);
    }

    private void OnSoundOptionClick()
    {
        options.Sound = !options.Sound;
        SoundOptionView.ShowOn(options.Sound);
        UpdateSound();
    }

    private void OnMusicOptionClick()
    {
        options.Music = !options.Music;
        MusicOptionView.ShowOn(options.Music);
        UpdateMusic();
    }

    private void UpdateSound()
    {
        if(options.Sound == true)
            AudioMixer.SetFloat("Sound", SoundVolumeMax);
        else
            AudioMixer.SetFloat("Sound", SoundVolumeMin);
    }

    private void UpdateMusic()
    {
        if (options.Music == true)
            AudioMixer.SetFloat("Music", MusicVolumeMax);
        else
            AudioMixer.SetFloat("Music", MusicVolumeMin);
    }
}