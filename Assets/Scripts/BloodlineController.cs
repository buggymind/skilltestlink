using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]

public class BloodlineController : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private Transform startTransform;
    private Transform endTransform;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void Init(Transform start, Transform end, Color color)
    {
        startTransform = start;
        endTransform = end;

        Show(color);
    }

    public void Show(Color color)
    {
        //lineRenderer.startColor = color;
        //lineRenderer.endColor = color;
        lineRenderer.SetPosition(0, startTransform.position);
        lineRenderer.SetPosition(1, endTransform.position);
    }
}
