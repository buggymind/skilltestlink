using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : UnitySingleton<DataManager>
{
    private SaveGame saveGame;

    private void Awake()
    {
        if (!ES3.FileExists())
            CreateNewSaveFile();
        else
            LoadSaveGame();
    }

    private void Start()
    {
        EventBus.Register(Events.Game.LevelFinished, OnLevelFinished);
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Game.LevelFinished, OnLevelFinished);
    }

    private void OnApplicationQuit()
    {
        SaveGame();
    }

    private void OnLevelFinished(object[] data)
    {
        SaveGame();
    }

    public LevelData GetLevelData(string sceneName)
    {
        var levelData = saveGame.LevelDatas.Find(ld => ld.SceneName == sceneName);

        if (levelData != null)
            return levelData;
        else
            return AddNewLevelData(sceneName);
    }

    public DailyRewardData GetDailyRewardData(string id)
    {
        var dailyRewardData = saveGame.DailyRewardDatas.Find(rd => rd.ID == id);

        if (dailyRewardData != null)
            return dailyRewardData;
        else
            return AddNewDailyRewardData(id);
    }

    public GameData GetGameData()
    {
        return saveGame.GameData;
    }

    public Options GetOptions()
    {
        return saveGame.Options;
    }

    private LevelData AddNewLevelData(string sceneName)
    {
        LevelData levelData = new LevelData() { SceneName = sceneName };
        saveGame.LevelDatas.Add(levelData);
        return levelData;
    }

    private DailyRewardData AddNewDailyRewardData(string id)
    {
        DailyRewardData dailyRewardData = new DailyRewardData() { ID = id };
        saveGame.DailyRewardDatas.Add(dailyRewardData);
        return dailyRewardData;
    }

    private void CreateNewSaveFile()
    {
        // Create a save game
        saveGame = new SaveGame();

        // Add first level and unlock it
        saveGame.LevelDatas = new List<LevelData>();
        var levelData = AddNewLevelData("Level_1");
        levelData.IsUnlocked = true;

        // Add daily rewards data
        saveGame.DailyRewardDatas = new List<DailyRewardData>();

        // Add game data
        saveGame.GameData = new GameData();

        // Add options
        saveGame.Options = new Options();

        // Save file to device
        SaveGame(); 
    }

    private void LoadSaveGame()
    {
        saveGame = ES3.Load<SaveGame>("SaveGame");
    }

    private void SaveGame()
    {
        ES3.Save("SaveGame", saveGame);
    }
}
