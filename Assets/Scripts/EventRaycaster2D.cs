using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventRaycaster2D : MonoBehaviour
{
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            var hit = GetRayHit();
            if (hit.collider != null)
                EventBus.Push(Events.Input.MouseDown, hit.transform);
        }

        if(Input.GetMouseButton(0))
        {
            var hit = GetRayHit();
            if(hit.collider != null)
                EventBus.Push(Events.Input.MouseHold, hit.transform);
        }

        if (Input.GetMouseButtonUp(0))
        {
            var hit = GetRayHit();
            if (hit.collider != null)
                EventBus.Push(Events.Input.MouseUp, hit.transform);
        }
    }

    private RaycastHit2D GetRayHit()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

        return Physics2D.Raycast(mousePos2D, Vector2.zero);
    }
}
