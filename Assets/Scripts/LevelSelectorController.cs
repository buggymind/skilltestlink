using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorController : MonoBehaviour
{
    [Header("References")]
    public StarsViewController StarsViewController;

    [Header("Settings")]
    public Color ColorLocked = Color.white;
    public Color ColorUnlocked = Color.red;

    public LevelData LevelData { get; private set; }

    public bool IsUnlocked = false;
    public string SceneName;

    private Image Image;
    private Button Button;

    private void Awake()
    {
        Button = GetComponent<Button>();
        Image = GetComponent<Image>();
    }

    private void Start()
    {
        InitSelector();

        Button.onClick.AddListener(OnButtonClick);
    }

    private void OnDestroy()
    {
        Button.onClick.RemoveAllListeners();
    }

    private void OnButtonClick()
    {
        if (IsUnlocked)
            LoadScene();
    }

    public void UnlockLevel()
    {
        LevelData.IsUnlocked = true;
    }

    private void InitSelector()
    {
        LevelData = DataManager.Instance.GetLevelData(SceneName);

        IsUnlocked = LevelData.IsUnlocked;
        if (IsUnlocked)
            Image.color = ColorUnlocked;
        else
            Image.color = ColorLocked;

        StarsViewController.HighlightStars(LevelData.CollectedBlood);
    }

    private void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
