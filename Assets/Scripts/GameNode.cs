using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class GameNode : MonoBehaviour
{
    public enum Types { Start, End, Multi }

    [Header("References")]
    public Text Info;

    [Header("Settings")]
    public Types Type;
    public int RequiredLinks;

    public bool IsFinished { get; set; }

    private Animator animator;
    new private Collider2D collider;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();
    }

    private void Start()
    {
        if (RequiredLinks > 0)
        {
            Info.text = RequiredLinks.ToString();
            Info.gameObject.SetActive(true);
        }
    }

    public void ShowSuccess()
    {
        animator.SetTrigger("Success");
        DisableCollider();

        if (RequiredLinks > 0)
            Info.gameObject.SetActive(false);
    }

    public void ShowLinked()
    {
        animator.SetTrigger("Linked");
    }

    public void Reset()
    {
        IsFinished = false;
        animator.SetTrigger("Reset");
    }

    public void DisableCollider()
    {
        collider.enabled = false;
    }
}
