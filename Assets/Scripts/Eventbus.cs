﻿using System;
using System.Collections.Generic;

public class EventBus
{
    public delegate void EventFunction(params object[] data);

    private static Dictionary<int, List<EventFunction>> EventListener { get; set; }

    static EventBus()
    {
        EventListener = new Dictionary<int, List<EventFunction>>();
    }

    internal static void Register(int fullBlood)
    {
        throw new NotImplementedException();
    }

    public static void Register(int eventType, EventFunction function)
    {
        List<EventFunction> eventFunctions;
        EventListener.TryGetValue(eventType, out eventFunctions);
        if (eventFunctions == null)
        {
            eventFunctions = new List<EventFunction>();
            EventListener.Add(eventType, eventFunctions);
        }
        eventFunctions.Add(function);
    }

    public static void Unregister(int eventType, EventFunction function)
    {
        List<EventFunction> eventFunctions;
        EventListener.TryGetValue(eventType, out eventFunctions);
        if (eventFunctions == null)
            return;

        eventFunctions.Remove(function);
    }

    public static void Push(int eventType, params object[] data)
    {
        List<EventFunction> eventFunctions;
        EventListener.TryGetValue(eventType, out eventFunctions);
        if (eventFunctions == null)
            return;

        List<EventFunction> deepCopy = new List<EventFunction>(eventFunctions);
        for (int i = 0; i < deepCopy.Count; i++)
            deepCopy[i](data);
    }
}