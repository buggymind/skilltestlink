using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreScreenViewController : MonoBehaviour
{
    [Header("Local references")]
    public Text InfoText;
    public Animator HeartAnimator;

    public void SetScore(int score)
    {
        HeartAnimator.SetInteger("Hearts", score);
    }

    public void SetTip(string tip)
    {
        InfoText.text = tip;
    }
}
