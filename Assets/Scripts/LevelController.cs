using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    private const string MapScene = "LevelMap";

    [Header("References")]
    public TimerView TimerView;
    public LevelProgressView LevelProgressView;
    public Animator CancelOverlayAnimator;

    [Header("Settings")]
    public int LevelTime = 20;

    private List<GameObject> levelObjects = new List<GameObject>();
    private int currentObjectIndex = 0;

    public int LevelObjectsCount { get; private set; }
    public int LinksFinished { get; private set; }

    private void Awake()
    {
        StoreChildGameObjects();
        LevelObjectsCount = levelObjects.Count;
    }

    void Start()
    {
        EventBus.Register(Events.Game.LinkFinished, OnLinkFinished);

        EnableNextObject();

        StartCoroutine(OutOfTime(LevelTime));
        TimerView.StartTimer(LevelTime);
    }

    void OnDestroy()
    {
        EventBus.Unregister(Events.Game.LinkFinished, OnLinkFinished);
    }

    private void OnLinkFinished(object[] data)
    {
        LinksFinished++;
        currentObjectIndex++;

        float progress = LinksFinished / (float)LevelObjectsCount;
        LevelProgressView.UpdateProgress(progress);

        if (currentObjectIndex < levelObjects.Count)
            EnableNextObject();
        else
            FinishLevel();
    }
    public void PlayAgain()
    {
        var activeScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(activeScene.name);
    }

    public void BackToMap()
    {
        SceneManager.LoadScene(MapScene);
    }

    public void CancelCurrentGame()
    {
        StopAllCoroutines();
        TimerView.StopTimer();
        CancelOverlayAnimator.SetTrigger("Show");
    }

    private IEnumerator OutOfTime(float time)
    {
        yield return new WaitForSeconds(time);
        FinishLevel();
    }

    private void StoreChildGameObjects()
    {
        foreach (Transform child in transform)
            levelObjects.Add(child.gameObject);
    }

    private void EnableNextObject()
    {
        levelObjects[currentObjectIndex].SetActive(true);
    }

    private int CalculateCollectedBlood()
    {
        // based on percentage 50, 75 and 90
        float percentage = (float)LinksFinished / LevelObjectsCount;

        if (percentage >= 0.9)
            return 3;
        else if (percentage >= 0.75)
            return 2;
        else if (percentage >= 0.5)
            return 1;
        else 
            return 0;
    }

    private void FinishLevel()
    {
        StopAllCoroutines();
        TimerView.StopTimer();

        var sceneName = SceneManager.GetActiveScene().name;
        var levelData = DataManager.Instance.GetLevelData(sceneName);
        var gameData = DataManager.Instance.GetGameData();

        var collectedBlood = CalculateCollectedBlood();
        var savedGameCollectedBlood = levelData.CollectedBlood;

        // Check if player got more points then last run
        if(collectedBlood > levelData.CollectedBlood)
        {
            int difference = collectedBlood - levelData.CollectedBlood;
            gameData.CurrentCollectedBlood += difference;
            gameData.TotalCollectedBlood += difference;
            levelData.CollectedBlood = collectedBlood;
        }

        // Check if player achieved the minimum score to complete the level for the first time
        if (collectedBlood > 0 && !levelData.IsCompleted)
        {
            levelData.IsCompleted = true;
            UnlockNextLevel();
        }

        EventBus.Push(Events.Game.LevelFinished, collectedBlood, savedGameCollectedBlood);
    }

    private void UnlockNextLevel()
    {
        var currentSceneName = SceneManager.GetActiveScene().name;
        int currentLevelId = int.Parse(currentSceneName.Substring(6, 1)); // e.g. from Level_1 we just want the 1
        int nextLevelId = currentLevelId + 1;
        string nextSceneName = string.Format("Level_{0}", nextLevelId);
        var levelData = DataManager.Instance.GetLevelData(nextSceneName);
        levelData.IsUnlocked = true;
    }
}
