public class Events 
{
    public class Input // 0  - 19
    {
        public const int MouseDown = 0;         // transform
        public const int MouseHold = 1;         // transform
        public const int MouseUp = 2;         // transform
    }

    public class Game  // 20 - 99
    {
        public const int LinkFinished = 20;
        public const int LevelFinished = 21;    // collectedBlood; saveGameCollectedBlood
        public const int OutOfTime = 22;
    }
}
