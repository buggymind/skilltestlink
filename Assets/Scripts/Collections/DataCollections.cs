using System.Collections.Generic;

public class SaveGame
{
    public List<LevelData> LevelDatas;
    public List<DailyRewardData> DailyRewardDatas;
    public GameData GameData;
    public Options Options;
}

public class LevelData
{
    public string SceneName;  // is used as the level id
    public int CollectedBlood = 0;
    public bool IsCompleted = false;
    public bool IsUnlocked = false;
}

public class GameData
{
    public System.DateTime LastDailyReset;
    public int TotalCollectedBlood;
    public int CurrentCollectedBlood;
    public int Donations;
    public int CollectedHearts;
}

public class DailyRewardData
{
    public string ID;
    public bool IsCollected;
}

public class Options
{
    public bool Sound = true;
    public bool Music = true;
}