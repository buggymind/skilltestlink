using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotSpotController : MonoBehaviour
{
    [Header("Global references")]
    public BloodlineController BloodlinePrefab;

    private List<GameNode> gameNodes;
    private List<BloodlineController> bloodlines = new List<BloodlineController>();

    private bool linkStarted;

    private void Awake()
    {
        FindGameNodesOnGameObject();
    }

    void Start()
    {
        EventBus.Register(Events.Input.MouseDown, OnMouseDown);
        EventBus.Register(Events.Input.MouseHold, OnMouseHold);
        EventBus.Register(Events.Input.MouseUp, OnMouseUp);

        CreateBloodlines();
    }

    private void OnDestroy()
    {
        EventBus.Unregister(Events.Input.MouseDown, OnMouseDown);
        EventBus.Unregister(Events.Input.MouseHold, OnMouseHold);
        EventBus.Unregister(Events.Input.MouseUp, OnMouseUp);
    }

    private void OnMouseDown(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNode.Type == GameNode.Types.Start)
            if (!linkStarted)
                StartLink();
    }

    private void OnMouseHold(object[] data)
    {
        Transform targetTransform = (Transform)data[0];
        var gameNode = targetTransform.GetComponent<GameNode>();

        if (gameNode.Type == GameNode.Types.End)
            if (gameNodes.Contains(gameNode))
                if (linkStarted && !gameNode.IsFinished)
                    FinishGameNode(gameNode);
    }

    private void FindGameNodesOnGameObject()
    {
        gameNodes = new List<GameNode>(GetComponentsInChildren<GameNode>(true));
    }

    private void FinishGameNode(GameNode gameNode)
    {
        gameNode.IsFinished = true;
        gameNode.ShowLinked();

        var gameNodesEnd = gameNodes.FindAll(gn => gn.Type == GameNode.Types.End);
        bool allFinished = gameNodesEnd.TrueForAll(gn => gn.IsFinished);

        if(allFinished)
            FinishLink();

        linkStarted = false;  // reset value so that the player has to click on start node again
    }

    private void OnMouseUp(object[] data)
    {
        linkStarted = false;
    }

    private void CreateBloodlines()
    {
        var gameNodeStart = gameNodes.Find(gn => gn.Type == GameNode.Types.Start);
        var gameNodesEnd = gameNodes.FindAll(gn => gn.Type == GameNode.Types.End);

        foreach (var gameNodeEnd in gameNodesEnd)
        {
            BloodlineController bloodlineController = Instantiate(BloodlinePrefab, this.transform, true);
            bloodlineController.Init(gameNodeStart.transform, gameNodeEnd.transform, Color.gray);
            bloodlines.Add(bloodlineController);
        }
    }

    private void StartLink()
    {
        linkStarted = true;
    }

    private void FinishLink()
    {
        gameNodes.ForEach(gn => gn.ShowSuccess());
        bloodlines.ForEach(b => Destroy(b.gameObject));

        EventBus.Push(Events.Game.LinkFinished);
    }
}
