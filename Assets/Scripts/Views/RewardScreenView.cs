using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardScreenView : MonoBehaviour
{
    public Button Button;
    public Image Image;
    public Text ThankYouText;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();   
    }

    public void SetData(DonationInfoDataScriptableObject donationInfoData)
    {
        Image.sprite = donationInfoData.RewardWindowImage;
        ThankYouText.text = donationInfoData.RewardWindowThankYouText;
    }

    public void Show(bool value)
    {
        animator.SetBool("Show", value);
    }
}
