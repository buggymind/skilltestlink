using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonView : MonoBehaviour
{
    [Header("References")]
    public Button Button;
    public Image Fill;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void ShowFullEffect(bool value)
    {
        animator.SetBool("HeartIsFull", value);
    }

    public void UpdateFill(int currentBlood, int requiredBlood)
    {
        float fillAmount = 1f / (float)requiredBlood * currentBlood;
        Fill.fillAmount = Mathf.Clamp(fillAmount, 0, 1);
    }
}
