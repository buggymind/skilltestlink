using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionElementView : MonoBehaviour
{
    public Sprite IconOn;
    public Sprite IconOff;

    public Image Image { get; private set; }
    public Button Button { get; private set; }

    private void Awake()
    {
        Image = GetComponent<Image>();
        Button = GetComponent<Button>();

        if (Image == null)
            Debug.LogError("OptionElementView is missing an Image component");

        if (Button == null)
            Debug.LogError("OptionElementView is missing a Button component");
    }

    public void ShowOn(bool value)
    {
        if (value == true)
            Image.sprite = IconOn;
        else
            Image.sprite = IconOff;
    }
}
