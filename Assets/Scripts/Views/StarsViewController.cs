using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarsViewController : MonoBehaviour
{
    [Header("References")]
    public List<Image> Stars;

    [Header("Settings")]
    public Color NormalColor;
    public Color HighlightColor;

    public void HighlightStars(int amount)
    {
        for (int i = 0; i < Stars.Count; i++)
        {
            if (i < amount)
                Stars[i].color = HighlightColor;
            else
                Stars[i].color = NormalColor;
        }
    }
}
