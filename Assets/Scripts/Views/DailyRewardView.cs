using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardView : MonoBehaviour
{
    [Header("References")]
    public Button Button;
    public Animator Animator;

    [Header("Settings")]
    public string PersistentUniqueID;

    public void SetActive(bool value)
    {
        Animator.SetBool("IsActive", value);
    }

    public void ShowEffect()
    {
        Animator.SetTrigger("OnTouch");
    }
}
