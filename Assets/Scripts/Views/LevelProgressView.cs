using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressView : MonoBehaviour
{
    public Image ProgressBar;

    public void UpdateProgress(float progress)
    {
        ProgressBar.fillAmount = progress;
    }
}
