using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileScoreView : MonoBehaviour
{
    public Text Text;

    public void SetCollectedHearts(int value)
    {
        Text.text = value.ToString();
    }
}
