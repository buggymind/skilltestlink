using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonationInfoView : MonoBehaviour
{
    public Text Text;
    public Button Button;
    public Image ButtonImage;
    public Image Icon;

    public DonationInfoDataScriptableObject DonationInfoData { get; private set; }

    public void SetInfoData(DonationInfoDataScriptableObject donationInfoData)
    {
        DonationInfoData = donationInfoData;

        Text.text = donationInfoData.DonationInfoText;
        ButtonImage.color = donationInfoData.DonationInfoColor;
        Icon.sprite = donationInfoData.DonationInfoSprite;
    }
}