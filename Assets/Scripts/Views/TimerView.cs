using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour
{
    public Text Timer;
    public Image ProgressCircle;

    private float timer;
    private float startTimer;
    private bool isRunning;

    public void StartTimer(float time)
    {
        startTimer = time;
        timer = time;
        isRunning = true;
        Timer.enabled = true;
    }

    public void StopTimer()
    {
        isRunning = false;
    }

    private void Update()
    {
        if(isRunning)
            timer -= Time.deltaTime;

        if (timer <= 0)
            isRunning = false;

        float progress = timer / startTimer;

        ProgressCircle.fillAmount = progress;
        Timer.text = ((int)timer).ToString();
    }
}
