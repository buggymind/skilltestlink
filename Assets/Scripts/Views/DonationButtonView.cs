using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonationButtonView : MonoBehaviour
{
    public Button Button { get; private set; }

    public DonationInfoDataScriptableObject DonationInfoData;
    
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        Button = GetComponent<Button>();
    }

    public void Show(bool value)
    {
        animator.SetBool("Show", value);
    }
}