using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonationOverlayView : MonoBehaviour
{
    public float ButtonDelay = 0.3f;

    public List<DonationButtonView> DonationButtonViews;

    void Awake()
    {
        DonationButtonViews = new List<DonationButtonView>(GetComponentsInChildren<DonationButtonView>());
    }

    public void Show(bool value)
    {
        StartCoroutine(ShowButtonsWithDelay(value));
    }

    private IEnumerator ShowButtonsWithDelay(bool value)
    {
        foreach (var donationButton in DonationButtonViews)
        {
            donationButton.Show(value);
            yield return new WaitForSeconds(ButtonDelay);
        }
    }
}
