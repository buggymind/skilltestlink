using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="DonationInfoData", menuName = "ScriptableObjects/DonationInfoData")]
public class DonationInfoDataScriptableObject : ScriptableObject
{
    [Header("Donation Info Window")]
    public Sprite DonationInfoSprite;
    public Color DonationInfoColor = Color.black;
    [Multiline] 
    public string DonationInfoText;

    [Header("Reward Window")]
    public Sprite RewardWindowImage;
    [Multiline] 
    public string RewardWindowThankYouText;
}
