using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TipData", menuName = "ScriptableObjects/TipData")]
public class TipsDataScriptableObject : ScriptableObject
{
    public List<string> Tips;
}
