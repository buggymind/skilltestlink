﻿using UnityEngine;

namespace Alpaca.Utils
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject);
        }
    }
}
