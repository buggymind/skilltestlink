using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OverviewMapUIController : MonoBehaviour
{
    private const string LevelScene = "LevelMap";

    public Animator StandardOverlay;
    public Animator DonationInfoWindow;
    public Animator ProfileWindow;

    public LevelButtonView LevelButtonView;
    public Button ProfileButton;

    public DonationOverlayView DonationOverlayView;
    public DonationInfoView DonationInfoView;
    public RewardScreenView RewardScreenView;
    public ProfileScoreView ProfileScoreView;

    public List<MapElement> MapElements;
    public List<DailyRewardView> DailyRewardViews;

    private GameData gameData;

    void Start()
    {
        // Register events
        LevelButtonView.Button.onClick.AddListener(OnLevelButtonClick);
        DonationOverlayView.DonationButtonViews.ForEach(view => view.Button.onClick.AddListener(delegate { OnDonationButtonClick(view); }));
        DonationInfoView.Button.onClick.AddListener(OnDonationInfoButtonClick);
        ProfileButton.onClick.AddListener(OnProfileButtonClick);
        RewardScreenView.Button.onClick.AddListener(OnRewardScreenClick);
        DailyRewardViews.ForEach(view => view.Button.onClick.AddListener(delegate { OnDailyRewardViewClick(view); }));

        // Load data
        gameData = DataManager.Instance.GetGameData();

        // Set start values
        StandardOverlay.SetBool("Show", true);
        DonationInfoWindow.SetBool("Show", false);
        ProfileWindow.SetBool("Show", false);
        RewardScreenView.Show(false);

        LevelButtonView.UpdateFill(gameData.CurrentCollectedBlood, Settings.RequiredBloodForDonation);
        ProfileScoreView.SetCollectedHearts(gameData.CollectedHearts);

        if (DayHasPassedSinceLastActivation())
            UnlockDailyRewards();

        if (PlayerHasRequiredBlood())
            LevelButtonView.ShowFullEffect(true);

        ShowInteractiveMapElements();
        ActivateDailyRewards();
    }

    private void OnDestroy()
    {
        LevelButtonView.Button.onClick.RemoveListener(OnLevelButtonClick);
        DonationOverlayView.DonationButtonViews.ForEach(view => view.Button.onClick.RemoveListener(delegate { OnDonationButtonClick(view); }));
        DonationInfoView.Button.onClick.RemoveListener(OnDonationInfoButtonClick);
        ProfileButton.onClick.RemoveListener(OnProfileButtonClick);
        RewardScreenView.Button.onClick.RemoveListener(OnRewardScreenClick);
        DailyRewardViews.ForEach(view => view.Button.onClick.RemoveListener(delegate { OnDailyRewardViewClick(view); }));
    }

    private void OnLevelButtonClick()
    {
        if (PlayerHasRequiredBlood())
        {
            StandardOverlay.SetBool("Show", false);
            DonationOverlayView.Show(true);
            ProfileWindow.SetBool("Show", false);
        }
        else
        {
            SceneManager.LoadScene(LevelScene);
        }
    }

    private void OnDonationButtonClick(DonationButtonView donationButtonView)
    {
        DonationInfoView.SetInfoData(donationButtonView.DonationInfoData);
        DonationInfoWindow.SetBool("Show", true);
    }

    private void OnDonationInfoButtonClick()
    {
        gameData.CurrentCollectedBlood -= Settings.RequiredBloodForDonation;
        gameData.Donations += 1;
        LevelButtonView.UpdateFill(gameData.CurrentCollectedBlood, Settings.RequiredBloodForDonation);
        ShowInteractiveMapElements();
        DonationInfoWindow.SetBool("Show", false);
        DonationOverlayView.Show(false);
        RewardScreenView.SetData(DonationInfoView.DonationInfoData);
        RewardScreenView.Show(true);
        LevelButtonView.ShowFullEffect(false);
    }

    private void OnProfileButtonClick()
    {
        bool currentState = ProfileWindow.GetBool("Show");
        ProfileWindow.SetBool("Show", !currentState);
    }

    private void OnRewardScreenClick()
    {
        RewardScreenView.Show(false);
        StandardOverlay.SetBool("Show", true);
    }

    private void OnDailyRewardViewClick(DailyRewardView view)
    {
        var dailyRewardData = DataManager.Instance.GetDailyRewardData(view.PersistentUniqueID);
        dailyRewardData.IsCollected = true;

        gameData.CollectedHearts += 1;

        view.ShowEffect();
        view.SetActive(false);

        ProfileScoreView.SetCollectedHearts(gameData.CollectedHearts);
    }

    private void ShowInteractiveMapElements()
    {
        foreach (var mapElement in MapElements)
        {
            if (gameData.Donations >= mapElement.RequiredDonations)
                mapElement.SceneReference.SetActive(true);
            else
                mapElement.SceneReference.SetActive(false);
        }

        ActivateDailyRewards();
    }

    private bool DayHasPassedSinceLastActivation()
    {
        DateTime lastLogin = gameData.LastDailyReset;
        TimeSpan difference = DateTime.UtcNow.Subtract(lastLogin);

        return difference.Days >= 1;
    }

    private void UnlockDailyRewards()
    {
        foreach (DailyRewardView dailyRewardView in DailyRewardViews)
        {
            var dailyRewardData = DataManager.Instance.GetDailyRewardData(dailyRewardView.PersistentUniqueID);
            dailyRewardData.IsCollected = false;
        }

        gameData.LastDailyReset = DateTime.UtcNow;
    }

    private void ActivateDailyRewards()
    {
        foreach (var dailyRewardView in DailyRewardViews)
        {
            var dailyRewardData = DataManager.Instance.GetDailyRewardData(dailyRewardView.PersistentUniqueID);
            if (dailyRewardData.IsCollected == false)
                dailyRewardView.SetActive(true);
        }
    }

    private bool PlayerHasRequiredBlood()
    {
        return gameData.CurrentCollectedBlood >= Settings.RequiredBloodForDonation;
    }

    [Serializable]
    public class MapElement
    {
        public int RequiredDonations;
        public GameObject SceneReference;
    }
}
